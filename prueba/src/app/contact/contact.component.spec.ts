import { ContactComponent } from './contact.component';
import { screen } from '@testing-library/angular';

describe('ContactComponent', () => {
  let component: ContactComponent;

  it('should create', () => {
    const component = new ContactComponent();

    expect(component).toBeTruthy();
  });

  it('should return a title', () => {});
  let string = 'Formulario De Contacto';

  const h3 = screen.findByText(string);
  console.log(h3);

  expect(h3).toEqual('Formulario De Contacto');
});
