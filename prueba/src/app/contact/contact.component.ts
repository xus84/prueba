import { Component, OnInit } from '@angular/core';

import { NgForm } from '@angular/forms';

interface ContactForm {
  email: string;
  receive: boolean;
  country: string;
  comment: string;
}

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css'],
})
export class ContactComponent implements OnInit {
  modalShift: boolean = false;

  model = {
    email: '',
    receive: false,
    country: '',
  };

  constructor() {}

  ngOnInit(): void {}

  openModal() {
    this.modalShift = true;
  }

  onSubmit(form: NgForm): void {
    console.log('Form values', form);
    this.openModal();
    setTimeout(() => {
      this.modalShift = false;
    }, 5000);
    form.resetForm();
  }
}

function reload() {
  throw new Error('Function not implemented.');
}
