import { AppComponent } from './app.component';

describe('AppComponent', () => {
  it(`should have as title 'thriflora'`, () => {
    const component = new AppComponent();
    const result = component.title;

    expect(result).toBe('thriflora');
  });

  it('should return current year', () => {
    const component = new AppComponent();

    const compiled = component.currentYear;

    expect(compiled).toEqual(2022);
  });
});
