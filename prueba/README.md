# Requirements:

## How to Install and Run the Project 🔧

### Use

It can be used online by accessing: https://thriflora.vercel.app/

Can be installed using git:

git clone https://gitlab.com/xus84/prueba

Run ng serve for a dev server. Navigate to http://localhost:4200/

# Docker:

It builds Docker images from a Dockerfile searching a Dockerfile in the path ".", and tagging the image with the name prueba

docker build -t prueba .

docker run --rm -it -v $(pwd)/src:/prueba/src -p 8080:4200 prueba

PORT:8080

# Docker compose commands:

build-app:
docker-compose build

start-app:
docker-compose up

down:
docker-compose down

# Test commands:

npm test
